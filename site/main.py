from fastapi import FastAPI, Form, Request, Depends
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles

from pydantic.dataclasses import dataclass
from typing import List

import json
from owlready2 import get_ontology

import urllib.parse


def format_name(text : str):
    return text[0].upper() + text[1:].replace('_', ' ')

def get_github_link(s, resource_name, ):
    encoded_file = urllib.parse.quote(s.encode('utf8'))
    filename = resource_name + ".json"
    github_link = "https://github.com/santiagohy/test/new/dev/resources/new?value=" + encoded_file + "&filename=" + filename
    return github_link

# Load ontology
onto = get_ontology("../ontology.owl").load()

# Getting data
tasks = [format_name(y) for x in onto['tâche'].instances() for y in x.nom_tâche] + ["Autre"]
task_types = list(set(value for x in onto['tâche'].instances() for value in onto['type_général_de_tâche'][x])) + ["Autre"]

resources = []
for x in onto['ressource'].subclasses():
    if list(x.subclasses()):        
        for y in x.subclasses():
            resources.append(format_name(y.name))
    else:
        resources.append(format_name(x.name))
resources.append("Autre")

# ------- Mounting App ------- #

app = FastAPI()

templates = Jinja2Templates(directory="templates")
app.mount("/static", StaticFiles(directory="static"), name="static")

@dataclass
class SimpleForm:
    name: str = Form()
    email: str = Form()
    personal_website: str = Form()
    resource_name: str = Form()
    resource_website: str = Form()
    resource_type: List[str] = Form()
    resource_description: str = Form()
    general_task: List[str] = Form()
    specific_task: List[str] = Form()
    task_description: str = Form()

# ------- GET methods ------- #

@app.get("/index", response_class=HTMLResponse)
async def read_item(request: Request):
    return templates.TemplateResponse(
        "index.html", {"request": request})

@app.get("/explore", response_class=HTMLResponse)
async def read_item(request: Request):
    return templates.TemplateResponse(
        "explore.html", {"request": request})

@app.get("/form", response_class=HTMLResponse)
async def read_item(request: Request):
    return templates.TemplateResponse(
        "form.html.jinja", {"request": request, "resources": resources, 
                            "tasks" : tasks, "task_types" : task_types})

@app.get("/form-1", response_class=HTMLResponse)
async def read_item(request: Request):
    return templates.TemplateResponse(
        "form.html.jinja", {"request": request})

@app.get("/download", response_class=HTMLResponse)
async def read_item(request: Request):
    return templates.TemplateResponse(
        "download.html", {"request": request})

@app.get("/about-atala", response_class=HTMLResponse)
async def read_item(request: Request):
    return templates.TemplateResponse(
        "about-atala.html", {"request": request})

# ------- POST methods ------- #

@app.post("/form-1", response_class=HTMLResponse)
async def read_message(request: Request, data: SimpleForm = Depends()):
    dict_data = vars(data)
    del dict_data['__pydantic_initialised__']
    jsn = json.dumps(dict_data, indent=4, ensure_ascii=False)
    github_link = get_github_link(jsn, data.resource_name)
    return templates.TemplateResponse(  
        "form-1.html.jinja", {"request": request, "data": data, "github_link": github_link})

