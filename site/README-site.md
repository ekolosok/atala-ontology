# Site


## Setup de l'environment

```bash
pip install virtualenv
python -m virtualenv .venv
source .venv/bin/activate
```

## Installer l'app
```bash
cd site
pip3 install -U -r requirements.txt
```

## Lancer l'app
```bash
uvicorn main:app --reload
```

**Go** to http://127.0.0.1:8000/index

## D'autres informations

Le site est fait avec bootstrap (https://getbootstrap.com/). On utilise le template "Clean Sky"  
Si on change des parties générales du site (ex. footer), le faire dans toutes les pages  
Les pages html et jinja templates doivent être dans le dossier "templates"  
Les scripts js, les fichiers css et autres doivent être dans le dossier static  

## ToDo #Santiago

Ajouter les classes "Autre" à l'ontologie  
Refaire fonction pour transformer le dictionnare dans owl  
Duexième partie du formulaire  
Vérfier si c'est necessaire de forker le répertoire  


## ToDo #Général

Nettoyer les pages, éliminer les references inutiles  
Ajouter l'image de l'ATALA au site  
Changer le nom de l'ontologie dans le fichier owl (via Protegé). Important! 
Verifier le noms de classes (ex. est_auteur_du_ressource)   
Mettre au même niveau les DPs et OPs   
