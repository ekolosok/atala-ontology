# Name
ATALA Ontology

# Description
NLP Tasks & Tools Ontology (in French).

# Usage
You need [Protégé](https://protege.stanford.edu/) installed to view the main .owl file. 

## Roadmap
We're curretly working on the ontology; a search tool based on it is planned after that.

## Contributing
For the moment, the only option of contributing is making a merge request to this repository. 
A no-code GUI for contribution is planned. 

## Authors and acknowledgment
ATALA ; Santiago Herrera, Elodie Venzeveren, Ekaterina Kolos

## Project status
In progress...
